let getCube = 5 ** 3
console.log(`The cube of 5 is ${getCube}`)


let address = ["285 Washington Ave NW", "California 90011"]
console.log(`I live at ${address[0]}, ${address[1]} `)

const animal = {
    Aname: "Lolong",
    type: "saltwater crocodile",
    weight: "1075kg",
    measure: "20ft 3 in"
}

console.log(`${animal.Aname} was a ${animal.type}. He weight ${animal.weight} with a measurement of ${animal.measure}`)

const numbers = [1 , 2 , 3 , 4 , 5]

numbers.forEach((number) => {
    console.log(`${number}`)
})


class Dog {
    constructor(name, age, breed) {
        this.name = name,
        this.age = age,
        this.breed = breed
    }
}

const myDog = new Dog ()

myDog.name = "Cloud"
myDog.age = 5
myDog.breed = "Belgian Malinois"

console.log(myDog)